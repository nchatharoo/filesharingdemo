//
//  ViewController.m
//  FileSharingDemo
//
//  Created by Nadheer Chatharoo on 11/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    marrFileData = [[NSMutableArray alloc] init];
    marrReceiveData = [[NSMutableArray alloc] init];
    image = [[UIImage alloc] init];
    
    UIButton *btnConnect = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x /2, self.view.frame.origin.y + 50, 100, 100)];
    btnConnect.backgroundColor = [UIColor blueColor];
    [btnConnect setTitle:@"Connect" forState:UIControlStateNormal];
    [btnConnect addTarget:self action:@selector(btnConnectClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnConnect];
    
    UIButton *btnSend = [[UIButton alloc] initWithFrame:CGRectMake(self.view.center.x /2, self.view.frame.origin.y +250, 100, 100)];
    btnSend.backgroundColor = [UIColor greenColor];
    [btnSend setTitle:@"Share" forState:UIControlStateNormal];
    [btnSend addTarget:self action:@selector(btnSendClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnSend];
    
	// Do any additional setup after loading the view, typically from a nib.
}

-(void)setUpMultipeer {
    static NSString * const XXServiceType = @"share";

    self.myPeerID = [[MCPeerID alloc] initWithDisplayName:[UIDevice currentDevice].name];
    
    self.mySession = [[MCSession alloc] initWithPeer:self.myPeerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
    self.mySession.delegate = self;
    
    self.browserVC = [[MCBrowserViewController alloc] initWithServiceType:XXServiceType session:self.mySession];
    self.browserVC.delegate = self;
    
    self.advertiser = [[MCAdvertiserAssistant alloc] initWithServiceType:XXServiceType discoveryInfo:nil session:self.mySession];
    [self.advertiser start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showBrowserVC
{
    [self presentViewController:self.browserVC animated:YES completion:nil];
}

-(void)dismissBrowserVC
{
    [self.browserVC dismissViewControllerAnimated:YES completion:nil];
}

-(void)stopWifiSharing:(BOOL)isClear
{
    if(isClear && self.mySession != nil){
        [self.mySession disconnect];
        
        [self.mySession setDelegate:nil];
        
        self.mySession = nil;
        
        self.browserVC = nil;
    }
}

-(void)btnConnectClicked:(id)sender {
    if (!self.mySession) {
        [self setUpMultipeer];
    } else {
        [self showBrowserVC];
    }
}

#pragma marks MCBrowserViewControllerDelegate
// Notifies the delegate, when the user taps the done button
-(void)browserViewControllerDidFinish:(MCBrowserViewController*)browserViewController {
    [self dismissBrowserVC];
    [marrReceiveData removeAllObjects];
}

// Notifies delegate that the user taps the cancel button.
-(void)browserViewControllerWasCancelled:(MCBrowserViewController *)browserViewController
{
    [self dismissBrowserVC];
}

-(void)sendData {
    [marrFileData removeAllObjects];
    
    UIImagePickerControllerSourceType sourceType = 0;
    sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        return;
    }
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.sourceType = sourceType;
    imagePicker.allowsEditing = YES;
    imagePicker.delegate = self;
    [self presentViewController:imagePicker animated:YES completion:nil];
    image = [UIImage imageNamed:[NSString stringWithFormat:@"%@", imagePicker]];
    NSLog(@"%@", image);
    
    NSData *sendData = UIImageJPEGRepresentation([UIImage imageNamed:@"dwightwide.jpg"], 0.5);
    NSUInteger length = [sendData length];
    NSUInteger chunkSize = 100 * 1024;
    NSUInteger offset = 0;

    do {
        NSUInteger thisChunkSize = length - offset > chunkSize ? chunkSize : length - offset;
        NSData* chunk = [NSData dataWithBytesNoCopy:(char *)[sendData bytes] + offset
                                             length:thisChunkSize
                                       freeWhenDone:NO];
        NSLog(@"chunk length : %lu",(unsigned long)chunk.length);
        
        [marrFileData addObject:[NSData dataWithData:chunk]];
        offset += thisChunkSize;
        // do something with chunk
    } while (offset < length);
    
    noOfData = [marrFileData count];
    noOfDataSend = 0;
    
    if ([marrFileData count] > 0) {
        [self.mySession sendData:[marrFileData objectAtIndex:noOfDataSend] toPeers:[self.mySession connectedPeers] withMode:MCSessionSendDataReliable error:nil];
    }
    NSLog(@"%@", image);
}

- (void)btnSendClicked:(id)sender
{
    [self sendData];
}

-(void)appendFileData
{
    NSMutableData *fileData = [NSMutableData data];

    for (int i = 0; i < [marrReceiveData count]; i++) {
        [fileData appendData:[marrReceiveData objectAtIndex:i]];
    }
    
    [fileData writeToFile:[NSString stringWithFormat:@"%@/Image.png", [NSHomeDirectory()stringByAppendingPathComponent:@"Documents"]] atomically:YES];
    UIImageWriteToSavedPhotosAlbum([UIImage imageWithData:fileData], self, @selector(image:didFinishSavingWithError:contextInfo:), nil);

}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (!error) {
        [self invokeAlertMethod:@"Successfully Sent" Body:@"Image shared successfully and saved in Cameraroll." Delegate:nil];
    } else {
        [[[UIAlertView alloc] initWithTitle:nil message:[error userInfo][@"error"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
    }

}

- (void)invokeAlertMethod:(NSString *)strTitle Body:(NSString *)strBody Delegate:(id)delegate
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTitle
                                                    message:strBody
                                                   delegate:delegate
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    alert = nil;
}

#pragma mark - MCSessionDelegate
- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    NSLog(@"data received : %lu",(unsigned long)data.length);
    
    if (data.length > 0) {
        if (data.length < 2) {
            noOfDataSend++;
            NSLog(@"noofdatasend : %d",noOfDataSend);
            NSLog(@"array count : %lu",(unsigned long)marrFileData.count);
            if (noOfDataSend < ([marrFileData count])) {
                [self.mySession sendData:[marrFileData objectAtIndex:noOfDataSend] toPeers:[self.mySession connectedPeers] withMode:MCSessionSendDataReliable error:nil];
            }else {
                [self.mySession sendData:[@"File Transfer Done" dataUsingEncoding:NSUTF8StringEncoding] toPeers:[self.mySession connectedPeers] withMode:MCSessionSendDataReliable error:nil];
            }
        } else {
            if ([[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] isEqualToString:@"File Transfer Done"]) {
                [self appendFileData];
            }else {
                [self.mySession sendData:[@"1" dataUsingEncoding:NSUTF8StringEncoding] toPeers:[self.mySession connectedPeers] withMode:MCSessionSendDataReliable error:nil];
                [marrReceiveData addObject:data];
            }
        }
    }
}

- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    NSLog(@"did receive stream");
}

- (void)session:(MCSession *)session didStartReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID withProgress:(NSProgress *)progress
{
    NSLog(@"start receiving");
}

- (void)session:(MCSession *)session didFinishReceivingResourceWithName:(NSString *)resourceName fromPeer:(MCPeerID *)peerID atURL:(NSURL *)localURL withError:(NSError *)error
{
    NSLog(@"finish receiving resource");
    
}

-(void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    NSLog(@"change state : %ld",state);
}

@end
