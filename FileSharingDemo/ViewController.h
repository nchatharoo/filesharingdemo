//
//  ViewController.h
//  FileSharingDemo
//
//  Created by Nadheer Chatharoo on 11/07/2014.
//  Copyright (c) 2014 Nadheer Chatharoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface ViewController : UIViewController <MCBrowserViewControllerDelegate, MCSessionDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    __block BOOL _isSendDate;
    NSMutableArray *marrFileData, *marrReceiveData;
    UIImage *image;
    int noOfData, noOfDataSend;
}

@property (strong, nonatomic) MCBrowserViewController *browserVC;
@property (strong, nonatomic) MCAdvertiserAssistant *advertiser;
@property (strong, nonatomic) MCSession *mySession;
@property (strong, nonatomic) MCPeerID *myPeerID;

@end
